class Usuario {
    constructor(nombre, apellido, libros, mascotas) {
        this._nombre = nombre;
        this._apellido = apellido;
        this._libros = libros;
        this._mascotas = mascotas;
    }

    getFullName() {
        return `${this._nombre} ${this._apellido}`;
    }

    addMascota(nombreMascota) {
        this._mascotas.push(nombreMascota);
    }

    countMascotas() {
        return this._mascotas.length;
    }

    addBook(titulo, nombreAutor) {
        this._libros.push({ nombre: titulo, autor: nombreAutor })
    }

    getBookNames() {
        return this._libros.map((libro) => libro.nombre)
    }
}

const libros = [
    {
        nombre: "El Jardin de las mariposas",
        autor: "Dot Hutchison"
    },
    {
        nombre: "Las rosas de Mayo",
        autor: "Dot Hutchison"
    }

]

const nuevoUser = new Usuario("Luca", "Bianchi", libros, ["Peces", "Gato"])

//Nombre
console.log(nuevoUser.getFullName());
//Libros
console.log(nuevoUser.getBookNames());
nuevoUser.addBook("El hombre del susurro", "Alex North")
console.log(nuevoUser.getBookNames());
//Mascotas
console.log(nuevoUser.countMascotas());
nuevoUser.addMascota('Flamingo');
console.log(nuevoUser.countMascotas());